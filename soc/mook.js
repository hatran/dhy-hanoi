var mook = {
    sobn: function () {
        let raw = `Phòng CNTT
        Phòng KHTH
        Khoa CĐHA`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `Máy trạm khoa CĐHA
        Máy chủ dữ liệu- PMC
        Máy chủ lưu trữ- PMC
        Máy chủ HNTH- PMC
        Máy trạm khoa nhi
        Máy trạm phòng TCKT
        Máy trạm khoa khám bệnh
        Máy trạm khoa xét nghiệm
        Máy trạm RHM
        Máy trạm khoa Mắt
        Máy trạm khoa TMH
        Máy chủ Pacs
        Máy chủ HIS
        Máy chủ LIS`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `Trung tâm nghiên cứu và đào tạo cán bộ 
        Phòng chống HIV/AIDS
        Trung tâm dịch vụ tổng hợp
        Trung tâm nghiên cứu Gen – Protein
        Viện đào tạo răng hàm mặt
        Viện đào tạo y học dự phòng và y tế công cộng
        Bệnh viện đại học y`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `Trung tâm nghiên cứu và đào tạo cán bộ 
        Phòng chống HIV/AIDS
        Trung tâm dịch vụ tổng hợp
        Trung tâm nghiên cứu Gen – Protein
        Viện đào tạo răng hàm mặt
        Viện đào tạo y học dự phòng và y tế công cộng
        Bệnh viện đại học y`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Văn phòng UBND TP
        Sở Giao thông Vận tải
        Sở Ngoại vụ
        Sở Nội vụ
        Sở Tài chính
        Sở Tư pháp
        Sở Xây dựng
        Thanh tra TP`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
